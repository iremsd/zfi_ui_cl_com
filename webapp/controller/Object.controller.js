sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"../model/formatter",
	"sap/m/MessageBox"
        ], function(BaseController, JSONModel, History, formatter, MessageBox) {
	"use strict";
        
	return BaseController.extend("com.arete.zfiuiclcom.controller.Object", {
	    formatter: formatter,
	    onInit: function() {
	        debugger;
	        var oViewModel = new JSONModel({
		Title: "",
		Baslik: [],
	        });
	        this.getRouter().getRoute("object").attachPatternMatched(this._onObjectMatched, this);
	        this.setModel(oViewModel, "objectView");
	    },
	    onNavBack: function() {
	        var sPreviousHash = History.getInstance().getPreviousHash();
        
	        if (sPreviousHash !== undefined) {
		history.go(-1);
	        } else {
		this.getRouter().navTo("worklist", {}, true);
	        }
	    },
	    _onObjectMatched: function(oEvent) {
	        debugger;
	        var sObjectId = oEvent.getParameter("arguments").DosyaNo,
		oViewModel = this.getModel("objectView"),
		satirModel = sap.ui.getCore().getModel("worklistView2"),
		oSatir = satirModel.getProperty("/Satir");
	        oViewModel.setProperty("/Baslik", oSatir);
	        oViewModel.setProperty("/Title", sObjectId);
	        this.onInitSmartFilterBar();
        
	    },
	    onBeforeRebindTable: function(oEvent) {
	        debugger;
	        this.getToplamTutar(); //getToplamTutar() çağır aşağıda fonks yaz
	    },
	    getToplamTutar: function(oEvent) {
	        debugger;
	        var oDataModel = this.getModel(),
		oViewModel = this.getModel("objectView"),
		dosyano = oViewModel.getProperty("/Baslik").DosyaNo,
		that = this;
	        oDataModel.callFunction("/DosyaToplamTutar", {
		urlParameters: {
		    DosyaNo: dosyano,
		},
		success: function(oData) {
		    debugger;
		    var oViewModel = that.getModel("objectView"),
		        sDmbtr = oViewModel.getProperty("/Baslik").Dmbtr;
		    oViewModel.setProperty("/Baslik/ToplamTutar", oData.ToplamTutar);
		    oViewModel.setProperty("/Baslik/Fark", sDmbtr - oData.ToplamTutar);
		},
		error: function(oData) {
        
        
		},
	        });
	    },
	    onInitSmartFilterBar: function(oEvent) {
	        debugger;
	        var smartFilter = this.getView().byId("smartFilterBarId"),
		smartTable = this.byId("smartTableId");
	        var dosyano = this.getModel("objectView").getProperty("/Baslik").DosyaNo;
	        var oDefaultFilter = {
		"DosyaNo": {
		    "ranges": [{
		        "exclude": false,
		        "operation": "EQ",
		        "keyField": "DosyaNo",
		        "value1": dosyano,
		        "tokenText": null
        
		    }]
		}
	        };
	        debugger;
	        smartFilter.setFilterData(oDefaultFilter);
	        smartTable.rebindTable();
	        debugger;
	    },
	    onPressDosya: function(oEvent) {
	        debugger;
	        var oDataModel = this.getModel(),
		oViewModel = this.getModel("objectView"),
		sBukrs = oViewModel.getProperty("/Baslik").Bukrs,
		sDosyaNo = oViewModel.getProperty("/Baslik").DosyaNo,
		durumKapali = oViewModel.getProperty("/Baslik").Kapali,
		durumTers = !durumKapali;
	        var oData = {
		Kapali: durumTers
	        }
	        debugger;
	        oDataModel.update("/TaahhutDosyaSet(Bukrs='" + sBukrs + "',DosyaNo='" + sDosyaNo + "')", oData, {
		success: function(data) {
		    debugger;
		    oDataModel.read("/TaahhutDosyaSet(Bukrs='" + sBukrs + "',DosyaNo='" + sDosyaNo + "')", {
		        success: function(data) {
			debugger;
			oViewModel.setProperty("/Baslik", data);
        
		        },
		        error: function(e) {
			debugger;
		        }
		    });
        
		},
		error: function(e) {
		    debugger;
		}
	        });
	    },
	    onPressKaydet: function(oEvent) {
	        debugger;
	        var oTable = this.byId("smartTableId").getTable(),
		oItems = oTable.getSelectedIndices();
        
	        if (!oItems.length) {
		sap.m.MessageToast.show("Lütfen bir satır seçiniz!");
	        } else {
		debugger;
		var arr = [],
		    that = this,
		    oDataModel = this.getModel(),
		    oViewModel = this.getModel("objectView"),
		    oData = {};
		oData.Operationid = "U";
		oData.TaahhutFatura = arr;
		for (var i = 0; i < oItems.length; i++) {
		    arr.push(oTable.getModel().getProperty(oTable.getContextByIndex(oTable.getSelectedIndices()[i]).sPath));
		}
		oDataModel.create("/OperationSet", oData, {
		    success: function(data) {
		        debugger;
		        var oTable = that.byId("smartTableId").getTable();
		        oTable.clearSelection();
		    },
		    error: function() {
		        debugger;
		        var oTable = that.byId("smartTableId").getTable();
		        oTable.clearSelection();
		    },
		});
	        }
	    },
	    onPressFaturaSil: function(oEvent) {
	        debugger;
	        var oTable = this.byId("smartTableId").getTable(),
		oItems = oTable.getSelectedIndices(),
		that = this;
        
	        if (!oItems.length) {
		sap.m.MessageToast.show("Lütfen bir satır seçiniz!");
	        } else {
		var sText = "Seçilen fatura silinecek.Onaylıyor musunuz?",
		    that = this;
		MessageBox.confirm(sText, {
		    title: "Teyit",
		    initialFocus: sap.m.MessageBox.Action.CANCEL,
		    onClose: function(sButton) {
		        if (sButton === MessageBox.Action.OK) {
			debugger;
			var arr = [],
			    oDataModel = that.getModel(),
			    oViewModel = that.getModel("objectView"),
			    oData = {};
			oData.Operationid = "D";
			oData.TaahhutFatura = arr;
			for (var i = 0; i < oItems.length; i++) {
			    arr.push(oTable.getModel().getProperty(oTable.getContextByIndex(oTable.getSelectedIndices()[i]).sPath));
			}
			oDataModel.create("/OperationSet", oData, {
			    success: function(data) {
			        debugger;
			        var oTable = that.byId("smartTableId").getTable();
			        oTable.clearSelection();
			    },
			    error: function() {
			        debugger;
			        var oTable = that.byId("smartTableId").getTable();
			        oTable.clearSelection();
			    },
			});
        
		        } else if (sButton === MessageBox.Action.CANCEL) {
        
		        } else if (sButton === "Custom Button") {
        
		        };
		    }
		});
	        }
	    }
	});
        
        });