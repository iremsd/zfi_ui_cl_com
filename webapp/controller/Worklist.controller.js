sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"../model/formatter",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageBox"
        
        ], function(BaseController, JSONModel, formatter, Filter, FilterOperator, MessageBox) {
	"use strict";
        
	return BaseController.extend("com.arete.zfiuiclcom.controller.Worklist", {
        
	    formatter: formatter,
        
	    onInit: function() {

	        var oViewModel;
	        oViewModel = new JSONModel({
		
		fragments: {
			DosyaNumarasi: "",
			BankaAdi: "",
			SirketKodu: "",
			Tutar:"",
			KrediKT: null,
			TaahhutKV: null,
			ParaBirimi:"",
			TaahhutTipi: "",
		}
	        });
	        this.setModel(oViewModel, "worklistView");
	        var satirModel=new JSONModel({
		Satir:[]
	        })
	        sap.ui.getCore().setModel(satirModel,"worklistView2");
	    },
	    onPress: function(oEvent) {
		    
		  var  satirModel=sap.ui.getCore().getModel("worklistView2");
		var satir=oEvent.getSource().getBindingContext().getObject();
		satirModel.setProperty("/Satir",satir);
	        this._showObject(oEvent.getSource());
	    },
	    onNavBack: function() {
	        history.go(-1);
	    },
	    onRefresh: function() {
		    debugger;
	        var oTable = this.byId("smartTableId");
	        oTable.getBinding("items").refresh();
	    },
        
	    _showObject: function(oItem) {
		    

	        this.getRouter().navTo("object", {
		DosyaNo: oItem.getBindingContext().getProperty("DosyaNo")
	        });
	    },
	    onPressDosyaSil: function(oEvent) {
	        debugger;
	        var oTable = this.byId("smartTableId").getTable(),
		oItems = oTable.getSelectedIndices(),
		oDataModel = this.getModel();
	        if (oItems.length === 0) {
		sap.m.MessageToast.show("Lütfen satır seçiniz!");
	        } else if (oItems.length > 1) {
		sap.m.MessageToast.show("Yalnızca bir satır seçiniz!");
	        } else if (oItems.length === 1) {
		debugger;
		var sText = "Seçilen dosya silinecek.Onaylıyor musunuz?",
		    that = this;
		MessageBox.confirm(sText, {
		    title: "Teyit",
		    initialFocus: sap.m.MessageBox.Action.CANCEL,
		    onClose: function(sButton) {
		        if (sButton === MessageBox.Action.OK) {
			debugger;
			var oDataModel = that.getModel(),
			    oTable = that.byId("smartTableId").getTable(),
			    oItems = oTable.getSelectedIndices(),
			    sBukrs = oTable.getModel().getProperty(oTable.getContextByIndex(oTable.getSelectedIndices()[0]).sPath).Bukrs,
			    sDosyaNo = oTable.getModel().getProperty(oTable.getContextByIndex(oTable.getSelectedIndices()[0]).sPath).DosyaNo;
			debugger;
			    oDataModel.remove("/TaahhutDosyaSet(Bukrs='" + sBukrs + "',DosyaNo='" + sDosyaNo + "')", {
			    method: "DELETE",
			    success: function(data) {
				   
			    },
			    error: function(e) {
				
			    }
			});
        
		        } else if (sButton === MessageBox.Action.CANCEL) {
        
		        } else if (sButton === "Custom Button") {
        
		        };
		    }
		});
        
	        }
	        debugger;
	    },
	    onPressDosyaEkle:function()
	    {
		    debugger;
		if (!this.newDialog) {
			this.newDialog = sap.ui.xmlfragment(
			    "com.arete.zfiuiclcom.view.fragment.Dialog",
			    this
			);
			this.getView().addDependent(this.newDialog);
		        }
	    
		        this.newDialog.open();
	    },
	    onCloseDialog:function()
	    {
		this.newDialog.close();    
	    },
	    onDialogDosyaEkle:function(oEvent)
	    {
		debugger;
		var that=this, 
		oDataModel=this.getModel(), 
		oViewModel=this.getModel("worklistView"),
		sDosyaNo=oViewModel.getData().fragments.DosyaNumarasi,
		sSirketKodu=oViewModel.getData().fragments.SirketKodu,
		sBankaAdi=oViewModel.getData().fragments.BankaAdi,
		sParaBirimi=oViewModel.getData().fragments.ParaBirimi,
		sTaahhutTip=oViewModel.getData().fragments.TaahhutTipi,
		sTutar=oViewModel.getData().fragments.Tutar,
		dKrediKT=oViewModel.getData().fragments.KrediKT,
		dTaahhutKV=oViewModel.getData().fragments.TaahhutKV;
		if(!sDosyaNo||!sSirketKodu||!sBankaAdi||!sParaBirimi||!sTaahhutTip||!sTutar||!dKrediKT||!dTaahhutKV)
		{
			debugger;
			sap.m.MessageToast.show("Lütfen tüm alanları doldurunuz!");
		}
		else
		{
			var oDataModel=that.getModel(),
			oViewModel=that.getModel("worklistView"),
			oNewEntry = {};
			oNewEntry.DosyaNo = sDosyaNo;
			oNewEntry.Bukrs = sSirketKodu;
			oNewEntry.Hbkid = sBankaAdi;
			oNewEntry.Waers = sParaBirimi;
			oNewEntry.TaahhutTipi = sTaahhutTip;
			oNewEntry.Dmbtr = sTutar;
			oNewEntry.KkTarih = dKrediKT;
			oNewEntry.Zfbdt = dTaahhutKV;
		debugger;
			oDataModel.create("/TaahhutDosyaSet", oNewEntry,
			    {
			        success: function (oSuccess) {
				debugger;
				alert("success");
			        },
			        error: function (oError) {
				debugger;
				alert("error");
			        }
			    });
			debugger;

		}

		
		
		debugger;
	    }
        
	});
        });